/**
 * TCD Software
 * Created by Kinga Jankowska on 21.04.17.
 */
(function () {
    'use strict';
    angular.module('app.settings')
        .config(function ($stateProvider) {
            $stateProvider
                .state(
                    'app.settings', {
                        url: '/settings',
                        templateUrl: 'app/settings/tpl/settings.html',
                        controller: 'SettingsController',
                        controllerAs: 'vm'
                    }
                )


        })
})();