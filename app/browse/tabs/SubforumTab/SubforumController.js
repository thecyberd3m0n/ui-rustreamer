/**
 * Created by tcd on 25.01.17.
 */
(function () {
    angular.module('app.browse')
        .controller('SubforumController', SubforumController);

    /**
     * @param {*} $stateParams
     * @param {RutrackerAPI} RutrackerAPI
     * @constructor
     * @export
     * @ngInject
     */
    function SubforumController($stateParams, RutrackerAPI) {
        'use strict';
        var vm = this;

        /**
         * @type {Array<torrentItem>}
         */
        vm.torrentList = [];
        /**
         * @param  {Array<torrentItem>} result
         * @return {void}
         */
        function onResult(result) {
            vm.torrentList = vm.torrentList.concat(result);
        }
        vm.getMore = function() {
            RutrackerAPI.getTorrentsInSubforum($stateParams.id,
                vm.torrentList.length)
                .then(onResult);
        };
        vm.getMore();
    }

})();
