/**
 * TCD Software
 * Created by Dmitrij Rysanow on 02.03.17.
 */
(function() {
    angular.module('app.browse', [
        'ui.router',
        'ngMaterial',
        'ngMdIcons',
        'ngPopover',
        'ngAnimate',
        'app.common']);
})();
