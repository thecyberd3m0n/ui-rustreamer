
/**
 * Created by kinia on 08.06.17.
 */
(function () {
    angular.module('app.common')
        .animation('.disappearance', disappearance);
    function disappearance() {
        return {
            enter: function (element, done) {
                TweenLite.to(element, 1, {autoAlpha:0, onComplete: done});
            }
        }
    }
})();