/**
 * Created by kinia on 20.05.17.
 */
(function () {
    angular.module('app.common')
        .directive('torrentList', torrentList);

    /**
     *
     * @param {!angular.scope} $rootScope
     * @returns {!angular.directive}
     * @ngInject
     */
    function torrentList($rootScope) {
        return {
            restrict: 'E',
            scope: {
                items: '='
            },
            templateUrl: 'app/common/directives/torrentList/torrentList.html',
            link: function(scope) {
                /**
                 * Sidebar for displaying torrent info
                 * @type {string}
                 * @const
                 */

                var OPEN_SIDENAV_EVENT = 'REQUEST_OPEN_INFOBAR';
                var CLOSE_SIDENAV_EVENT = 'REQUEST_CLOSE_INFOBAR';

                scope.infoSidenav = null;
                scope.selectedItem = null;

                scope.openInfo = function(entry) {
                    scope.selectedItem = entry;
                    $rootScope.$broadcast(OPEN_SIDENAV_EVENT, entry);
                };

                scope.$on('deselect', function () {
                   scope.selectedItem = null;
                });

                scope.isOpenRight = function(){
                    return $mdSidenav(TORRENT_INFO_SIDEBAR_ID).isOpen()
                };
            }
        }
    }}


)();
