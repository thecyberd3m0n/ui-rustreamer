/**
 * TCD Software
 * Created by Dmitrij Rysanow on 25.12.16.
 */
(function () {
    angular.module('app.common')
        .directive('videoitem', VideoItem);
    /**
     *
     * @param {PlayerService} PlayerService
     * @return {{restrict: string, scope: string, templateUrl: string, link: link}}
     */
    function VideoItem(PlayerService) {
        'use strict';
        return {
            restrict: 'A',
            scope: '&',
            templateUrl: 'app/common/directives/torrentItems/videoItem/videoitem.html',
            link: function(scope, element, attr) {
                scope.DEFAULT_ICON = 'local_movies';
                scope.PLAY_ICON = 'play_arrow';
                scope.INFO_ICON = 'info_outline';
                scope.PAUSE_ICON = 'pause';

                scope.currentIcon = scope.DEFAULT_ICON;

                scope.switchIcon = function(icon) {
                    scope.currentIcon = icon;
                };

                scope.play = function($event) {
                    $event.stopPropagation();
                    PlayerService.startStream(scope.entry);
                };
            }
        };
    }
})();
