/**
 * Created by tcd-primaris on 04.06.17.
 */
(function () {
    angular.module('app.common')
        .factory('Track', Track);

    /**
     * @param {object} args
     * @constructor
     */
    function Track() {
        function Track(args) {
            this.id = 0;
            this.name = '';
            this.length = 0;
            this.mime = '';
            this.link = '';
            this.index = '';
            this.metadata = '';

            if (args) {
                var fields = Object.keys(this);
                for (var i in fields) {
                    if (args.hasOwnProperty(fields[i])) {
                        this[fields[i]] = args[fields[i]];
                    }
                }
            }
        }
        return Track;
    }
})();