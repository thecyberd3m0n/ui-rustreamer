/**
 * TCD Software
 * Created by Dmitrij Rysanow on 12.03.17.
 */
(function() {
    angular.module('app.common')
        .provider('Environment', Environment)
        .constant('ENVIRONMENTS', {
                'NWJS': 'NWJS',
                'Browser': 'Browser'
            });
    /**
     * Detects which environment is running
     *
     * @param {ENVIRONMENTS} ENVIRONMENTS
     * @return {angular.factory}
     * @constructor
     */
    function Environment(ENVIRONMENTS) {
        var current = '';
        function init() {
            var isNode = (typeof process !== 'undefined' && typeof require !== 'undefined');
            var isNodeWebkit = false;
            if (isNode) {
                //If so, test for Node-Webkit
                try {
                    isNodeWebkit = (typeof require('nw.gui') !== 'undefined');
                } catch (e) {
                    isNodeWebkit = false;
                }
            }
            if (isNodeWebkit) {
                current = ENVIRONMENTS.NWJS;
            } else {
                current = ENVIRONMENTS.Browser;
            }
        }
        function getCurrent() {
            return current;
        }
        init();
        return {
            getCurrent: getCurrent,
            $get: function() {
                return {
                    getCurrent: function() {
                        return current;
                    }
                };
            }
        };
    }
})();
